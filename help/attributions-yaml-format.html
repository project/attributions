<p>YAML is a human-readable data serialization format.  The Drupal Git
Repository Usage Policy requires assets contained in a repo to be
specified in a file named <code>ASSETS.yml</code>. That uses a subset
of this format for metadata.</p>

<p>For more information about this format, see:</p>
<ul>
<li><a href="http://yaml.org/">YAML.org</a></li>
<li><a href="https://en.wikipedia.org/wiki/YAML">Wikipedia: YAML</a></li>
</ul>

<h2 id ="syntax">Syntax</h2>

<p>The data structure hierarchy in YAML is maintained by outline
indentation.  For attributions, each resource are represented by an
unique (in this file) identifier, followed by colon+newline followed
by an intented list of keys and values separated by colon+space:</p>

<pre>
identifier:
  key: value
  key: value
  &hellip;
</pre>

<p>For example, the resource identified by “images/griffin_head.jpg”,
with associated keys and values, may look like this:</p>

<pre>
images/griffin_head.jpg:
  type: asset
  approved: allowed license
  title: Griffin Head
  attributionText: Paul Keller
  attributionUrl: http://flickr.com/people/paulk/
  license: CC BY 2.0
  rights: http://creativecommons.org/licenses/by/2.0/deed.no
  source: http://flickr.com/photos/paulk/2062848161/
</pre>

<p>In general there is no need to put quotes around strings, but there are exceptions:</p>
<ul>
<li>Use quotes to force a string, e.g. if your key or value is <code>10</code> but you want it to return a String and not a Fixnum, write <code>'10'</code> or <code>"10"</code>.</li>
<li>Use quotes if your value includes special indicators, such as a colon (<code>:</code>) followed by a space or a newline, or <code>{</code>, <code>}</code>, <code>[</code>, <code>]</code>, <code>,</code>, <code>&</code>, <code>*</code>, <code>#</code>, <code>?</code>, <code>|</code>, <code>+</code>, <code>-</code>, <code>&lt;&lt;</code>, <code>&gt;&gt;</code>, <code>=</code>, <code>!</code>, <code>%</code>, <code>@</code>, <code>\</code>.</li>
<li>Single quotes let you put almost any character in your string, and won't try to parse escape codes. <code>'\n'</code> would be returned as the string <code>\n</code>.</li>
<li>Double quotes parse escape codes. For instance, <code>"\n"</code> will be returned as a line feed character.</li>
<li>If <code>&gt;</code> is used to indicate a scalar, indicators are not interpreted (so no quotes are required).
</ul>

<h3>Identifier</h3>

<p>Each resource entry should be identified with an unique (in this
file) string that identify the resource. This string should be the
path (from the root of the repo) to an individual file, or the path to
a directory. In the latter case the entry applies to all the files
that live below the directory, unless overriden by more specific
(single file) entries.

<p>The path may contain the star wildcard <code>*</code> (meaning any
string, including the null string).</p>

<p>This line must not be indented.</p>

<p>A hash character (<code>#</code>), followed by an arbitrary string
may be appended to the path to create multiple entries for the same
directory or file.  This can be used in the case where several assets
or source code fragments, each associated with
a <em>different</em> <code>attributionText</code>, etc. is aggregated
in a single directory or file.</p>

<p>The path may contain the star wildcard * (meaning any string,
including the null string).</p>

<p>The following identifier says that the entry applies to all the
files that lives below the “images”-directory:</p>

<pre>
images:
</pre>

<p>The following identifier says that the entry applies to the single
file “griffin_head.jpg” that is in the “images”-directory:</p>

<pre>
images/griffin_head.jpg:
</pre>

<p>The following identifier says that the entry applies to all the
files that lives in a directory named “originals”, located below the
“images”-directory:</p>

<pre>
images/*/originals:
</pre>

<p>The following is used to attribute a file named “slider.js” that is
made up of tow different components, the first authored by “John
Smith”, the second by “Dojo Inc”.</p>

<pre>
lib/slider.js#foo:
  attributionText: John Smith

lib/slider.js#bar:
  attributionText: Dojo Inc
</pre>



<h3>Administrative keys</h3>

<p>Two keys are used for administrative purposes:</p>

<ul>
<li><code>type</code>: Must be set to '<code>code</code>', '<code>asset</code>' or '<code>comment</code>'.</li>
<li><code>approved</code>: Must be link to LWG issue tracker or the string '<code>allowed license</code>'.</li>
</ul>

<p>The <code>type</code> should have the value <code>code</code> if
the resource is program code, and <code>asset</code> if the resource
is a non-code asset (e.g. text, image, video, audio, font, icon).</p>

<p>The <code>type</code> key <em>must</em> always be present in an entry.  If
the <code>type</code> of the entry is <code>comment</code> no other
keys are required.</p>


<h3>Metadata keys</h3>

<p>The following set of keys may be used to document the source,
license and provenance, of a resource:</p>

<ul>
<li><code>title</code>: A human readable string identifying the licensed work (recommended).</li>
<li><code>source</code>: Source of licensed work. This may be an URL, or a plain text string explaining the provenance of the work.</li>
<li><code>license</code>: Human readable name of license or policy of the work (required).</li>
<li><code>copyrightNotice</code>: The copyright notice for the (possibly adapted) resource, or directions for finding such information provided by other means.</li>
<li><code>attributionText</code>: Author (person, project or organization) to receive attribution.</li>
<li><code>attributionUrl</code>: URL author's profile or home page.  The author may be an individual, an organization, or a project.</li>
<li><code>rights</code>: URL to license or policy document for resource (required unless the license is one of our approved licenses).</li>
</ul>

<p>Unless the <code>type</code> of the entry is <code>comment</code>,
those marked “required” must be present in the entry.  The other keys
are optional.  If they are present, they may help downstream
recipients understand how to fulfil the attribution requirement of
licenses such as the Creative Commons licenses, but it is OK for a
project to provide for this by other means.  If you do, it may be
helpful to use text bound to the key <code>copyrightNotice</code> to provide
directions for finding such information.</p>


<h3>Metadata keys for adaptions</h3>

<p>If the work is an adapation, then the following additional keys may
be used to document its provenance.  Only <code>originalLicense</code>
is <em>required</em> by the LWG.</p>


<ul>
<li><code>originalTitle</code>: A human readable string identifying the original of the adapted work.</li>
<li><code>originalLicense</code>: Human readable name of license giving permission to adapt the work (required).</li>
<li><code>originalCopyrightNotice</code>: The copyright notice for the original work</li>
<li><code>originalAttributionText</code>: The name of the author of original that is adapted</li>
<li><code>originalAttributionUrl</code>: URL to profile or home page of author of original.</li>
<li><code>originalRights</code>: URL to license or policy document for original.</li>
</ul>


<p>Again, the required key is necessary to fulfil the
documentation requirements of the Drupal.org Git Repository policy.
The other keys may be helpful for downstream recipients to determine the
provenance of a resource.</p>

<h3>Presentation keys</h3>

<p>The <strong>Attributions</strong> module will use the values bound
to the metadata keys to provide text and links for the automatically
generated attribution page and block.  In addution,
the <strong>Attributions</strong> module recognizes the following keys
that is only used to structure the presentation of the metadata:</p>

<ul>
<li><code>template</code>: Defines a template string that may be used as a template for condtructing the attribution text.</li>
<li><code>weight</code>: an optional integer that determines the weight of the resource.</li>
<li><code>hide</code>: set to TRUE to hide the resource.</li>
<li><code>path</code>: show the attribution block only on pages matching path (regexp)</li>
</ul>

<p>The key <code>template</code> can be used to specify a text
template for a custom attribution.  You may use metadata keys,
prefixed with the sigil <code>@</code>, to get the value of those keys
inserted into the template.  The string <code>GPLv2+</code> is quoted
to escape the <code>+</code>. There is no need to quote the template,
even if it contains the YAML reserved indicator <code>@</code>,
because the <code>&gt;</code> indicates that this is a string.</p>

<pre>
sites/all/libraries/acmelib;
  type: code
  approved: https://www.drupal.org/node/123456
  license: 'GPLv2+'
  title: ACME Library
  attributionText: ACME Corporation
  template: >
    @title for PHP by @attributionText is relicensed for Drupal
    under GPLv2+ with permission from the authors.
</pre>

  
<p>The key <code>weight</code> accepts an integer value. It can be
used to indicate the placement of the resource relative to other
resources. The default <code>weight</code> value is 0, so you may a
negative value to place the resource on top.  In the example below, a
comment in the <strong>Attributions Demo</strong>
module's <code>ASSETS.yml</code> is assigned a negative weight value
to make sure it is displayed before any of the (bogus) attributions
shown by that module:</p>

<pre>
comment:
  type: comment
  weight: -1
  template: >
    The following list of attributions are only displayed for demo
    purposes. None of these resources are actually used by this
    module:
</pre>


<p>The key <code>hide</code> with a value set to <code>TRUE</code> can
be used to indicate that the resource should <em>not</em> appear on
the attribution page or in the attribution block.  The use case for
this key is to hide third party materials that need to be documented
in <code>ASSETS.yml</code> per Drupal.org Git Repo policy, but need
not not appear in an attribution block on a site (e.g. resources that
does not carry a public attribution requirement, such as MIT, GPL+FE,
public domain or CC zero). Example of use:</p>

<pre>
images/emptypurse.jpg:
  type: asset
  approved: allowed license
  hide: TRUE
  source: https://openclipart.org/detail/196212/empty-purse---portemonnaie
  license: public domain
  rights: https://openclipart.org/share
</pre>

<p>The key <code>path</code> may be used to specify a regular
expression that must be matched for the attribution to appear in the
attribution block.  In this example, the attribution will only appear
in the attribution block if the path to the page
matches <code>help_example/history</code>:</p>

<pre>
help/180px-Lerdorf.jpg:
  type: asset
  approved: allowed license
  attributionText: Jud Dagnall
  source: http://en.wikipedia.org/wiki/File:Lerdorf.jpg
  title: Rasmus Lerdorf
  license: CC BY-SA 3.0
  rights: http://creativecommons.org/licenses/by-sa/3.0/deed.en
  path: 'help_example\/history'
</pre>

<p>Note that since the value assigned to <code>path</code> is a
regular expression, the slash (<code>/</code>) must be escaped.</p>


<h2 id="troubleshooting">Troubleshooting</h2>

<p>If your YAML does not parse, you may use one of the following online
tools to check the validity of your YAML syntax:</p>

<ul>
<li><a href="http://yaml-online-parser.appspot.com/">Online YAML Parser</a></li>
<li><a href="http://www.yamllint.com/">YAML Lint</a></li>
</ul>
