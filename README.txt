ATTRIBUTIONS README.txt
=======================

CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Recommended modules
* Installation
* Maintainers

INTRODUCTION

The Attributions module may be used by maintainers of Drupal
modules and themes to create an attribution block or an attribution
page that can be used to display the license and attribution
information about materials (such as code libraries, fonts, icons and
images) that are included in a project distribution, but is not
licensed under the Drupal.org mandatory license for code: GPLv2+.

The declarations concerning licenses and attributions are pulled from
files named ASSETS.yml.

Please note that this module will do nothing on its own. Only download
and install it if it is listed as required or recommended by another
module or theme.

There is also an Attributions Demo module to demonstrate the use
of this module.

REQUIREMENTS

* Advanced help hint[1]:
  Will link link standard help text to online help and advanced help.
* Composer Manager[2]:
  This module allows modules to depend on PHP libraries managed by
  Composer.

RECOMMENDED MODULES

* Advanced Help[3]:
  When this module is enabled the administrator will have access to
  more extensive help.

INSTALLATION

This module has dependencies on an external library that is managed by
Composer Manager.  It is strongly recommended to use drush to enable
the module:

  drush en attributions

If you don't use drush, you need to use the composer CLI tool to
install and update the library.  Navigate to the “Composer File
directory” as configured in the Composer Manager's Settings page and
run the following two commands:

  composer install --no-dev
  composer update --no-dev

See also: Composer Manager for Drupal 6 and Drupal 7[4] and Installing
modules[5].

MAINTAINER

* gisle (https://www.drupal.org/u/gisle)

Any help with development (patches, reviews, comments) are welcome.

[1]: https://www.drupal.org/project/advanced_help_hint
[2]: https://www.drupal.org/project/composer_manager
[3]: https://www.drupal.org/project/advanced_help
[4]: https://www.drupal.org/node/2405805
[5]: https://drupal.org/documentation/install/modules-themes/modules-7

